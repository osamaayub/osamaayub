<p> Hi there 👋</p>

<table>
<thead>
<th>
  
<img src="https://github-readme-streak-stats.herokuapp.com/?user=osamaayub&theme=tokyonight"></th>
<th><img align="center" src="https://github-readme-stats.vercel.app/api/top-langs/?username=osamaayub&layout=compact&theme=tokyonight" alt="osama" /></th>
  <th><img src="https://github-readme-stats.vercel.app/api?username=osamaayub&theme=tokyonight"></th>
</thead>
</table>
<hr>

### Badges
[![Python Badge](https://img.shields.io/badge/-Python-61DBFB?style=for-the-badge&labelColor=black&logo=Python&logoColor=61DBFB)](#) 
[![C++ Badge](https://img.shields.io/badge/-C++-007acc?style=for-the-badge&labelColor=Red&logo=C++&logoColor=007acc)
[![C_Language_Badge](https://img.shields.io/badge/-C-007ace?style=for-the-badge&labelColor=black&logo=C&logoColor=007acc)](#)
[![HTML_Language_Badge](https://img.shields.io/badge/-HTML-=F0DB4F?style=for-the-badge&labelColor=orange&logo=HTML&logoColor=F0DB4F)](#)
<hr>
###Community Badge
<a href="https://twitter.com/osamaayub9" ><img src="https://img.shields.io/twitter/follow/osamaayub.svg?style=social" /> </a>
  <hr>
 ###![(ConnectWith me)]
 <thead>
  <br>
 <a href="https://www.twitter.com/in/osamaayub9/"><img src="assets/twitter.png" width="50px;"></a> &nbsp;&nbsp;&nbsp;&nbsp;
 <a href="https://www.instagram.com/ayub.683/"><img src="assets/insta.png" width="50px;"></a> &nbsp;&nbsp;&nbsp;&nbsp;
 <a href="https://www.linkedin.com/in/osama-ayub-9aba58175/"><img src="assets/linkdln.png" width="50px;"></a> &nbsp;&nbsp;&nbsp;&nbsp;
 <a href="https://www.youtube.com/channel/UC56Q2bWaSRApOoBv877W2Dg"> <img src="assets/youtube.png" width="50px;"></a> &nbsp;&nbsp;&nbsp;&nbsp;
  <a href="https://www.facebook.com/profile/Osama Ayub/"><img src="assets/facebook.png" width="50px;"></a> &nbsp;&nbsp;&nbsp;&nbsp
  <a href="https://www.github.com/osamaayub/"><img src="assets/github.png" width="50px;"></a> &nbsp;&nbsp;&nbsp;&nbsp
  
  
  <p> <img src="https://komarev.com/ghpvc/?username=osamaayub" alt="osama" /> </p>

  <br>

#### Profile Visits 
![visitors](https://visitor-badge.glitch.me/badge?page_id=osama.ayub)
<br>
 #### Coding Stats

<!--START_SECTION:osamaayub-->
```text
C++ Language        1 hr 50 mins    ██▒░░░░░░░░░░░░░░░░░░░░░░   09.61 % 
C Language          1 hr 27 mins    ██░░░░░░░░░░░░░░░░░░░░░░░   07.63 % 
python               1 hr 20 mins        ░░░░░░░░░░░░░░░░░░░░░░░░░   00.19 % 
HTML                1 hr 10 mins
```
<!--END_SECTION:osamaayub-->
  
<table>

  <thead>
    <td>Languages</td>
    <td><img src="Skills/vscode-icons_file-type-python.png" width="60px;"></td>
    <td><img src="Skills/logos_c.png" width="60px;"></td>
    <td><img src="Skills/logos_c-plusplus.png" width="60px;"></td>
     <td><img src="Skills/logos_git-icon.png" width="30px;"></td>
    <td><img src="Skills/logos_github-octocat.png" width="30px;"></td>
    <td><img src="Skills/Assembly.jpg" width="60px;"></td>
    <td><img src="Skills/css.png" width="60px;"></td>
    <td><img src="Skills/Html.png" width="60px;"></td>
     </thead>

  <tr>
    <td>Database</td>
    <td><img src="Skills/vscode-icons_file-type-mongo.png" width="45px;"></td>
     <td><img src="Skills/Sql.png" width="30px;"></td>
     <td><img src="Skills/logos_mysql.png" width="30px;"></td>
  </tr>
  




